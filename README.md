


# Running
To debug: './start_vision.sh'     - Calls a simple debugging interface, using a pygame window display. 
          './start_vision_gui.sh' - Calls a complete debuggin interface, using a GUI

To start a vision server that will listen for stategy clients: './start_vision_server.sh'



# Files
* __init__.py
* mpcam.py
* MplayerCam.py
* Exceptions.py
* HelperFunctions.py
* Filtering.py
* ImagePreprocess.py
* FrameCapture.py
* ObjectParameters.py
* ObjectDetection.py
* ObjectOrientation.py
* VisionInterface.py
* VisionDebugger.py
* VisionDisplay.py
* VisionGui.py
* VisionMain.py
* Main



# Parameters
All the parameters are stored in 'Vision/Parameters/'.
Separate .xml files have been created for each pitch.
