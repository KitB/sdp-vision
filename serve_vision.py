# Serve vision just runs a vision server
from vision_server import VisionServerFactory
from optparse import OptionParser
from twisted.internet import reactor

# Get arguments from command line
parser = OptionParser()
parser.add_option("-P", "--pitch",\
        dest="pitch", action="store", type="int", help="Choose pitch to run on, one of {0, 1}", default=0)
parser.add_option("-p", "--port",\
        dest="port", action="store", type="int", help="Which port to listen on", default=31410)
parser.add_option("-H", "--host",\
        dest="host", action="store", type="string", help="Which host to listen on", default='')
parser.add_option("-m", "--use-mplayer",\
        dest="use_mplayer", action="store_true", help="Use mplayer for vision input rather than SimpleCV",\
        default=False)
parser.add_option("-v", "--video-device-number",\
        dest="videoDev", action="store", type="int", help="The number of the /dev/video* device to use", default=0)
(options, args) = parser.parse_args()
if not options.pitch in [0,1]:
    parser.error("Pitch must be one of: {0,1}")

if not options.videoDev in [-2,-1,0,1,2]:
    parser.error("Pitch must be one of: {-2..2}")

print "Looking at pitch: %d"%options.pitch
print "Using video device: %d"%options.videoDev
print "Listening on port %d"%options.port
try:
    server = VisionServerFactory(pitch = options.pitch, use_mplayer = options.use_mplayer, video_device_number=options.videoDev)
    reactor.listenTCP(options.port, server, interface=options.host)
    reactor.run()
except KeyboardInterrupt:
    sys.stdout.write("\rUser exited\n")
    sys.stdout.flush()
server.close()
