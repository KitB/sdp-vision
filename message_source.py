""" Generates messages to be sent via socket """
# XXX MUST GUARANTEE MESSAGES ARE SAME LENGTH

import socket
import struct

def _buildString(type, one, two, _):
    """ Makes a human-readable string """
    str = "%c(%03d,%03d)"%(type, one, two)
    assert len(str)==10
    return str

def _buildPacket(type, one, two, three):
    """ Creates a packet like a string """
    try:
        c = struct.pack('cHHf', type, one, two, three)
    except struct.error:
        # there's a bug to be fixed here
        c = struct.pack('cHHf', type, 0, 0, 0)
        
    assert len(c)==12
    return c

def _buildDimensionPacket(cx, cy, dx, dy):
    """ Creates a packet in which to send the dimensions of the pitch """
    try:
        c = struct.pack('cHHHH', 'd', cx, cy, dx, dy)
    except struct.error:
        # Something went wrong
        c = struct.pack('cHHHH', 'd', 0, 0, 0, 0)
    return c

class MessageSource:
    def __init__(self, host, tryPort, humanReadable=False):
        """ Create a new instance with a given host,
        trying ports starting at tryPort until it finds one.
        """

        self._build = _buildString if humanReadable else _buildPacket
        self._serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = int(tryPort)

        gotSocket = False
        # Keep trying to get sockets until we find one that is unused
        while not gotSocket:
            try:
                self._serverSocket.bind((host, port))
                gotSocket = True
            except socket.error:
                port += 1
        self._port = port

    def waitForClient(self):
        """ Blocks until a client connects """
        self._serverSocket.listen(2)
        (self._clientSocket, (self._clientAddr, _)) = self._serverSocket.accept()
        print "Got client from %s"%self._clientAddr

    def getRequest(self):
        """ Blocks until the client sends a request """
        try:
            data = self._clientSocket.recv(1)
            assert len(data)>0, "Data from socket was empty"
            # struct.unpack returns a tuple no matter what
            (r,) = struct.unpack("b", data)
            return r
        except socket.timeout as (str, _):
            print "Socket timed out:\n%s"%str
        except socket.error as (str, _):
            print "Client disconnected:\n%s"%str
        except AssertionError, e:
            print "Assertion failed, client probably disconnected:\n%s"%e
            return None
    def sendBall(self, x, y):
        """ Send the position of the ball """
        try:
            self._clientSocket.send(self._build('r', x, y, 0))
        except TypeError:
            pass
    def sendYellowBot(self, x, y, a):
        """ Send the position and angle of the yellow robot """
        try:
            self._clientSocket.send(self._build('y', x, y, a))
        except TypeError:
            pass
    def sendBlueBot(self, x, y, a):
        """ Send the position and angle of the blue robot """
        try:
            self._clientSocket.send(self._build('b', x, y, a))
        except TypeError:
            pass
    def sendDimensions(self, cx, cy, dx, dy):
        """ Send the dimensions of the pitch """
        print "Sending dimensions"
        try:
            self._clientSocket.send(_buildDimensionPacket(cx, cy, dx, dy))
        except TypeError as e:
            print "ERROR: %s" % e
            pass

    def getPort(self):
        """ Returns the port number that the server is listening on """
        return self._port

    def sendExit(self):
        """ Tell the client to exit """
        self._clientSocket.send('exit')
        self._clientSocket.close()
        self._serverSocket.close()
