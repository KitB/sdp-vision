from pygame.time import *
from SimpleCV import *
from MplayerCam import MplayerCamera



class FrameCapture:
    
    # Initialize a FrameCapture
    def __init__(self, pre_processor, use_mplayer=False, videoDevice=1):
        
        self.__pre_processor = pre_processor
        self.__use_mplayer = use_mplayer
        self.__videoDevice = videoDevice
        self.__camera = None
        self.__clock = None
        
        # Prepare variables to store frames
        self.__frame_hsv = []
        self.__frame_rgb = []



    # Return the appropriate camera
    def __get_camera(self):
        if not self.__camera:
            if self.__use_mplayer:
                print "Using Mplayer Capture"
                self.__camera = MplayerCamera(self.__getVideoDevice())
            else:
                print "Using SimpleCV Capture"
                self.__camera = Camera(self.__getVideoDevice())
        
        # Camera object
        return self.__camera



    # Return current video device (dev/video*) - 0 or 1
    def __getVideoDevice(self):
        
        # Video device object
        return self.__videoDevice



    # Return the current clock
    def __get_clock(self):
        if not self.__clock:
            self.__clock = Clock()
        
        # Clock object
        return self.__clock



    # Return a frame in RGB
    def get_frame_rgb(self):
        if not self.__frame_rgb:
            self.__get_clock().tick()
            self.__frame_rgb = self.__get_camera().getImage()
            self.__frame_rgb = self.__pre_processor.doRotUnd(self.__frame_rgb)
        
        # Image
        return self.__frame_rgb



    # Return a frame in HSV
    def get_frame_hsv(self):
        if not self.__frame_hsv:
            self.__frame_hsv = self.get_frame_rgb().toHSV()
        
        # Image
        return self.__frame_hsv



    # Go to next frame
    def nextFrame(self):
        self.__frame_hsv = []
        self.__frame_rgb = []



    # Return frame rate
    def get_fps(self):
        
        # Float
        return self.__clock.get_fps()



