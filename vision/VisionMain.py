import os
from FrameCapture import *
from ImagePreprocess import *
from ObjectParameters import *
from VisionInterface import *



# Main vision function. Loop through frame, calculate information
def visionMain(mediator):
    
    # For brevity we'll use m for the mediator
    m = mediator
    
    # Also for brevity. Later we'll set up some names
    bp, bpt, ba, bat = (m._pos_blue_bot, m._pos_blue_bot_t, m._orientation_blue_bot, m._orientation_blue_bot_t)
    yp, ypt, ya, yat = (m._pos_yellow_bot, m._pos_yellow_bot_t, m._orientation_yellow_bot, m._orientation_yellow_bot_t)
    ballp, ballt = (m._pos_ball, m._pos_ball_t)
    
    path = os.path.join(m._path, "Parameters", "pitch_" + str(m._pitch))
    
    # These are the objects (obj)
    yellow_robot = Object("yellow_robot", path)
    blue_robot = Object("blue_robot", path)
    ball = Object("ball", path)
    yellow_circle = Object("yellow_circle", path)
    blue_circle = Object("blue_circle", path)
    parameters = Object("pre_process", path)
    pitch = Object("pitch", path)
    
    # There are the classes used
    preprocessor = pre_processor(640, 480, parameters)
    frame_capture = FrameCapture(preprocessor, m._use_mplayer, m._videoDev)
    vision_interface = VisionInterface(preprocessor, frame_capture)
    
    # Information holders
    coords_yellow_robot = None
    coords_blue_robot = None
    coords_ball = None
    
    # Calculate pitch coordinates
    (cx, cy, dx, dy) = pitch.get_pitch_dims()
    m._dimensions[0] = cx
    m._dimensions[1] = cy
    m._dimensions[2] = dx
    m._dimensions[3] = dy



    while True:
        
        # Timestamps
        timestamp = time.time()
        
        # Yellow robot
        binary_image_yellow_robot, coords_yellow_robot, coords_yellow_robot_cropped = vision_interface.get_coordinates(coords_yellow_robot, yellow_robot, None, False)
        binary_image_yellow_circle, coords_yellow_circle, coords_yellow_circle_cropped = vision_interface.get_coordinates(coords_yellow_robot, yellow_circle, binary_image_yellow_robot,  True)
        orientation_yellow_robot = vision_interface.get_orientation(coords_yellow_circle, coords_yellow_robot)
        
        # Blue robot
        binary_image_blue_robot, coords_blue_robot, coords_blue_robot_cropped = vision_interface.get_coordinates(coords_blue_robot, blue_robot, None, False)
        binary_image_blue_circle, coords_blue_circle, coords_blue_circle_cropped = vision_interface.get_coordinates(coords_blue_robot, blue_circle, binary_image_blue_robot,  True)
        orientation_blue_robot = vision_interface.get_orientation(coords_blue_circle, coords_blue_robot)
        
        # Ball
        binary_image_ball, coords_ball, coords_ball_cropped = vision_interface.get_coordinates(coords_ball, ball, None, False)
        
        # Move to next frame
        vision_interface.nextFrame()
        
        # Calculate fps
        fps = vision_interface.get_frame_rate()
        
        # Adding timestamps
        if coords_yellow_robot:
            (yp[0], yp[1], ypt.value) = int(coords_yellow_robot[0]), int(coords_yellow_robot[1]), timestamp
        if coords_blue_robot:
            (bp[0], bp[1], bpt.value) = int(coords_blue_robot[0]), int(coords_blue_robot[1]), timestamp
        if coords_ball:
            (ballp[0], ballp[1], ballt.value) = int(coords_ball[0]), int(coords_ball[1]), timestamp
        if orientation_yellow_robot:
            (ya.value, yat.value) = orientation_yellow_robot, timestamp
        if orientation_blue_robot:
            (ba.value, bat.value) = orientation_blue_robot, timestamp



