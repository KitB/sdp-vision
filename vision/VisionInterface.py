import os
from ObjectDetection import *
from ObjectOrientation import *
from SimpleCV import *



class VisionInterface:
    
    # Initialize the VisionInterface
    def __init__(self, pre_processor, frame_capture):
        self.pre_processor = pre_processor
        self.frame_capture = frame_capture
        self.croped_image_rgb = []
        self.croped_image_hsv = []
        self.full_image_rgb = []
        self.full_image_hsv = []



    # Return the full, undistorted, rotated image
    def get_full_image_rgb(self):
        if not self.full_image_rgb:
            self.full_image_rgb = self.frame_capture.get_frame_rgb()
        
        # Image
        return self.full_image_rgb



    # Return the full, undistorted, rotated image in HSV
    def get_full_image_hsv(self):
        if not self.full_image_hsv:
            self.full_image_hsv = self.frame_capture.get_frame_hsv()
        
        # Image
        return self.full_image_hsv



    # Return the full image, croped
    def get_croped_image_rgb(self):
        if not self.croped_image_rgb:
            self.pre_processor.load_default_parameters()
            img = self.get_full_image_rgb()
            self.croped_image_rgb = self.pre_processor.doCrop(img)
        
        # Image
        return self.croped_image_rgb



    # Return the full image, croped in HSV
    def get_croped_image_hsv(self):
        if not self.croped_image_hsv:
            self.croped_image_hsv = self.get_croped_image_rgb().toHSV()
        
        # Image
        return self.croped_image_hsv



    # Return the binary image of an object, croped around the pitch
    def get_full_binary_image(self, obj):
        img = self.get_croped_image_hsv()
        
        # Binary Image
        return get_obj_image(img, obj)



    # Return the binary image of an object, croped around a point, and the new coordinates of that point
    def get_partial_binary_image(self, obj, point):
        img = self.get_full_image_hsv()
        img, mappedPoint = self.pre_processor.get_fixed_crop(img, point, obj)
        
        # Binary Image
        return get_obj_image(img, obj), mappedPoint



    # Calculate the coordinates of an object
    # Crop around 'point' to find the 'obj'. If a 'circle' do the ''robot_binary_image test
    def get_coordinates(self, point, obj, robot_binary_image, isCircle):
        
        # Return binary image of obj
        if point:
            self.pre_processor.load_default_parameters()
            binary_image_obj, mappedPoint = self.get_partial_binary_image(obj, point)
        else:
            mappedPoint = None
            self.pre_processor.load_default_parameters()
            binary_image_obj = self.get_full_binary_image(obj)
        
        # Return coordinates of object
        fromCroped = True
        coords_obj_cropped = get_obj_blob_coords(binary_image_obj, obj, mappedPoint, robot_binary_image, isCircle)
        coords_obj = self.pre_processor.get_mapped_coords(coords_obj_cropped, fromCroped)

        # Image, Point(On full image), Point(On cropped image)
        return binary_image_obj, coords_obj, coords_obj_cropped



    # Calculate the orientation of an object
    def get_orientation(self, circle_coords, robot_coords):
        
        # Return the angle between the two points
        orientation_robot = get_obj_orientation(circle_coords, robot_coords)
        
        # Angle (radians)
        return orientation_robot



    # Get the frame rate
    def get_frame_rate(self):
        
        # Float
        return self.frame_capture.get_fps()



    # Move to the next frame
    def nextFrame(self):
        self.frame_capture.nextFrame()
        self.croped_image_rgb = []
        self.croped_image_hsv = []
        self.full_image_rgb = []
        self.full_image_hsv = []



