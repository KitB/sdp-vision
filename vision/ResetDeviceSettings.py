from MplayerCam import *

# List of devices
Devices = [1,2]

# Start Mplayer which resets settings, and then kill it.
for device in Devices:
    Camera = MplayerCamera(device)
    Camera.kill_mplayer()
