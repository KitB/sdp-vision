import os
from FrameCapture import *
from ImagePreprocess import *
from ObjectParameters import *
from VisionInterface import *



class VisionDebugger:
    
    # Initialize the VisionDebugger
    def __init__(self, path, pitch=0, use_mplayer=False, videoDevice=0):
        self.path = os.path.join(path, "Parameters", "pitch_" + str(pitch))
        
        # These are the objects (obj)
        self.yellow_robot = Object("yellow_robot", self.path)
        self.blue_robot = Object("blue_robot", self.path)
        self.ball = Object("ball", self.path)
        self.yellow_circle = Object("yellow_circle", self.path)
        self.blue_circle = Object("blue_circle", self.path)
        self.parameters = Object("pre_process", self.path)
        self.pitch = Object("pitch", self.path)
        
        # There are the classes used
        self.preprocessor = pre_processor(640, 480, self.parameters)
        self.frame_capture = FrameCapture(self.preprocessor, use_mplayer, videoDevice)
        self.vision_interface = VisionInterface(self.preprocessor, self.frame_capture)
        
        # Information holders
        self.coords_yellow_robot = None
        self.coords_blue_robot = None
        self.coords_ball = None
        self.dimensions = [None]*4
        
        # Calculate pitch coordinates
        (cx, cy, dx, dy) = self.pitch.get_pitch_dims()
        self.dimensions[0] = cx
        self.dimensions[1] = cy
        self.dimensions[2] = dx
        self.dimensions[3] = dy


    # Used by VisionDisplay to calculate everything for a single frame
    def doCalculations(self):
        
        # Full, undistorted, rotated image
        image = self.vision_interface.get_full_image_rgb()
        
        # Full, undistorted, rotated, HSV image
        hsv_image = self.vision_interface.get_full_image_hsv()
        
        # Croped, undistorted, rotated image
        croped_image = self.vision_interface.get_croped_image_rgb()
        
        # Yellow robot
        binary_image_yellow_robot, self.coords_yellow_robot, coords_yellow_robot_cropped = self.vision_interface.get_coordinates(self.coords_yellow_robot, self.yellow_robot, None, False)
        binary_image_yellow_circle, coords_yellow_circle, coords_yellow_circle_cropped = self.vision_interface.get_coordinates(self.coords_yellow_robot, self.yellow_circle, binary_image_yellow_robot, True)
        orientation_yellow_robot = self.vision_interface.get_orientation(coords_yellow_circle, self.coords_yellow_robot)
        
        # Blue robot
        binary_image_blue_robot, self.coords_blue_robot, coords_blue_robot_cropped = self.vision_interface.get_coordinates(self.coords_blue_robot, self.blue_robot, None, False)
        binary_image_blue_circle, coords_blue_circle, coords_blue_circle_cropped = self.vision_interface.get_coordinates(self.coords_blue_robot, self.blue_circle, binary_image_blue_robot,True)
        orientation_blue_robot = self.vision_interface.get_orientation(coords_blue_circle, self.coords_blue_robot)
        
        # Ball
        binary_image_ball, self.coords_ball, coords_ball_cropped = self.vision_interface.get_coordinates(self.coords_ball, self.ball, None, False)
        
        # Move to next frame
        self.vision_interface.nextFrame()
        
        # Calculate fps
        fps = self.vision_interface.get_frame_rate()
        
        # Images, Points, Floats
        return image, hsv_image, croped_image, binary_image_yellow_robot, binary_image_yellow_circle, \
               binary_image_blue_robot, binary_image_blue_circle, binary_image_ball, \
               self.coords_yellow_robot, coords_yellow_circle, self.coords_blue_robot, coords_blue_circle,\
               self.coords_ball, coords_yellow_robot_cropped, coords_blue_robot_cropped, coords_ball_cropped,\
               orientation_yellow_robot, orientation_blue_robot, fps



    # Load default settings
    def load_default_parameters(self):
        self.yellow_robot.load_parameters()
        self.blue_robot.load_parameters()
        self.ball.load_parameters()
        self.yellow_circle.load_parameters()
        self.blue_circle.load_parameters()
        self.parameters.load_parameters()
        self.preprocessor.load_default_parameters()



    # Reload new settings, if xml has changed
    def reload_parameters(self):
        self.yellow_robot = Object("yellow_robot", self.path)
        self.blue_robot = Object("blue_robot", self.path)
        self.ball = Object("ball", self.path)
        self.yellow_circle = Object("yellow_circle", self.path)
        self.blue_circle = Object("blue_circle", self.path)
        self.parameters = Object("pre_process", self.path)
        self.preprocessor = pre_processor(640, 480, self.parameters)
        self.vision_interface = VisionInterface(self.preprocessor, self.frame_capture)



