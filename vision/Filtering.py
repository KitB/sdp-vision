import operator
from math import *
from SimpleCV import *
from HelperFunctions import *



# Select largest (in area) blob
# obj <- instance of Recognizable Object
def satisfiesAreaProps(blob, obj):
    area = blob.area()
    (min_area, max_area) = obj.get_area()
    
    # Boolean
    if area >= min_area and area <= max_area:
        return blob.centroid()
    return None



# Properties that the circle blob should have
def satisfiesCircleProps(circle_blob, robot_centroid, obj):
    circle_centroid = satisfiesAreaProps(circle_blob, obj)
    
    if circle_centroid:
        centroidsDist = obj.get_centroidsDist()
        circleProperty = obj.get_circleProperty()
        
        # Perform actual checks
        distanceCheck = closenessCheck(circle_centroid, robot_centroid, centroidsDist)
        circleCheck = circle_blob.isCircle(circleProperty)
        
        # Point or None
        if distanceCheck and circleCheck:
            return circle_centroid
    return None



# Properties that the orientation vector should have
def satisfiesOrientationProps(point_from, point_to, robot_binary_image, obj):
    
    # Extend the vector between the two points, by scalarValue
    scalarValue = obj.get_orientationScalar()
    vector = map(operator.sub, point_to, point_from)
    scaled_vector = map(lambda x: x*scalarValue, vector)
    scaled_point = map(operator.add, point_to, scaled_vector)
    
    # Checking the pixel value of the above point is in the robot blob
    scaled_point = map(int, scaled_point)
    pixel = robot_binary_image.getPixel(scaled_point[0] , scaled_point[1])
    if pixel:
        
        # Boolean
        return pixel[0] > 0 #sum(pixel) > 250
    return None



