import os
from SimpleCV import *
from Exceptions import *
from xml.dom import minidom



class Object:
    
    # Initialize an object
    def __init__(self, name, path = ''):
        
        self.__name = name
        self.__path = path
        
        try:
            
            # Create full path to that object's file. Read all nodes
            self.__full_path = minidom.parse(os.path.join(self.__path, self.__name + ".xml") )
            
            # Retrieve childs or root
            nodes = self.__full_path.childNodes[0].childNodes
            
            
            
            for node in nodes:
                
                # Case 1: Ball, Robot, Black circle, Pitch
                if node.nodeName == "colour":
                    self.__colour_nodes = node.childNodes
                    self.__type = "Object"
                    
                elif node.nodeName == "objectProperties":
                    self.__property_nodes = node.childNodes
                    self.__type = "Object"
                    
                # Case 2: Pre-process
                elif node.nodeName == "pre_process":
                    self.__preProcess_nodes = node.childNodes
                    self.__type = "Pre-process"
            self.load_parameters()
            
        except IOError:
            raise DataNotFound("Default values not found in " + os.path.join(path, self.__name) )



    # Load default parameters
    def load_parameters(self):
        
        # Case 1: Ball, Robot, Black circle, Pitch
        if self.__type == "Object":
            
            # For each colour sub-node, store the information
            for node in self.__colour_nodes:
                
                if node.nodeName == "colour":
                    self.__colour = ( int(node.getAttribute("value")),
                                      int(node.getAttribute("saturation")),
                                      int(node.getAttribute("hue")) )
                elif node.nodeName == "tolerance":
                    self.__tolerance = ( int(node.getAttribute("value")), 
                                         int(node.getAttribute("saturation")),
                                         int(node.getAttribute("hue")) )
                elif node.nodeName == "noise":
                    self.__noise = ( int(node.getAttribute("value")) )
            
            
                
            # For each property sub-node, store the information
            for node in self.__property_nodes:
                
                if node.nodeName == "area":
                    self.__area = ( int(node.getAttribute("min")),
                                    int(node.getAttribute("max")) )
                elif node.nodeName == "circleProperty":
                    self.__circleProperty = ( float(node.getAttribute("value")) )
                elif node.nodeName == "centroidsDist":
                    self.__centroidsDist = ( int(node.getAttribute("value")) )
                elif node.nodeName == "orientationProperty":
                    self.__orientationScalar = ( float(node.getAttribute("value")) )
                elif node.nodeName == "pitch_dims":
                    self.__pitch_dims = ( int(node.getAttribute("cx")),
                                          int(node.getAttribute("cy")),
                                          int(node.getAttribute("dx")),
                                          int(node.getAttribute("dy")) )
                elif node.nodeName == "crop_around_by":
                    self.__crop_around_by = (int(node.getAttribute("value")) )
                elif node.nodeName == "columns":
                    self.__columns = (int(node.getAttribute("value")) )
                elif node.nodeName == "rows":
                    self.__rows = (int(node.getAttribute("value")) )
        
        
        
        # Case 2: Pre-process
        if self.__type == "Pre-process":
            
            # For each pre-process property node, store the information
            for node in self.__preProcess_nodes:
                
                if node.nodeName == "crop_position":
                    self.__crop_position = ( int(node.getAttribute("x")),
                                             int(node.getAttribute("y")) )
                elif node.nodeName == "crop_size":
                    self.__crop_size = ( int(node.getAttribute("height")),
                                         int(node.getAttribute("width")) )
                elif node.nodeName == "camera_pos":
                    self.__camera_pos = ( int(node.getAttribute("x")),
                                          int(node.getAttribute("y")) )
                elif node.nodeName == "rotate":
                    self.__rotate = ( float(node.getAttribute("angle")) )
                elif node.nodeName == "camera_height":
                    self.__camera_height = ( float(node.getAttribute("meters")) )
                elif node.nodeName == "robot_height":
                    self.__robot_height = ( float(node.getAttribute("meters")) )



    # Write on XML
    def write_value(self, name, top_left_x, top_left_y, bot_right_x, bot_right_y, average_value):
        element = self.__full_path.createElement(name)
        element.setAttribute("top_left_x", str(top_left_x))
        element.setAttribute("top_left_y", str(top_left_y))
        element.setAttribute("bot_right_x", str(bot_right_x))
        element.setAttribute("bot_right_y", str(bot_right_y))
        node = self.__full_path.childNodes[0].childNodes[4]
        node.appendChild(element)
        doc = open(os.path.join(self.__path, self.__name + ".xml"),'w')
        self.__full_path.writexml(doc)
        doc.close()

        




    # Get parameters
    def get_name(self):
        
        # String
        return self.__name

    def get_colour(self):
        
        # Tuple of integers
        return self.__colour

    def get_tolerance(self):

        
        # Tuple of integers
        return self.__tolerance

    def get_noise(self):

        
        # Integer
        return self.__noise

    def get_area(self):

        
        # Tuple of integers
        return self.__area
        
    def get_circleProperty(self):

        
        # Float
        return self.__circleProperty
        
    def get_centroidsDist(self):
        
        # Integer
        return self.__centroidsDist

    def get_orientationScalar(self):

        
        # Float
        return self.__orientationScalar

    def get_crop_position(self):
        
        # Integer
        return self.__crop_position

    def get_crop_size(self):
        
        # Integer
        return self.__crop_size

    def get_camera_pos(self):

        
        # Integer
        return self.__camera_pos

    def get_pitch_dims(self):

        
        # Integer
        return self.__pitch_dims

    def get_rotate(self):

        
        # Float
        return self.__rotate

    def get_camera_height(self):


        
        # Integer
        return self.__camera_height

    def get_robot_height(self):

        
        # Float
        return self.__robot_height

    def get_cropAroundBy(self):

        
        # Integer
        return self.__crop_around_by
    
    def get_columns(self):
        
        # Integer
        return self.__columns
    
    def get_rows(self):
        
        # Integer
        return self.__rows



    # Set parameters
    def set_colour(self, colour):
        self.__colour = (int(round(colour[2])), int(round(colour[1])), int(round(colour[0])))

    def set_tolerance(self, tolerance): 
        self.__tolerance = (int(round(tolerance[2])), int(round(tolerance[1])), int(round(tolerance[1])))

    def set_noise(self, noise):
        self.__noise = int(noise)

    def set_area(self, area):
        if area[0] >= area[1]:
            raise "Error - the min area of object should be smaller than the max area"
        self.__area = (area[0], area[1])

    def set_circleProperty(self, circleProperty):
        self.__circleProperty = float(circleProperty)

    def set_centroidsDist(self, centroidDist):
        self.__centroidsDist = int(centroidDist)

    def set_orientationScalar(self, orientationScalar):
        self.__orientationScalar = float(orientationScalar)

    def set_crop_position(self, position, add_x, add_y):
        self.__crop_position = (int(position[0]) + add_x, int(position[1] + add_y))

    def set_crop_size(self, size, add_x, add_y):
        self.__crop_size = (int(size[0]) + int(add_x) , int(size[1]) + int(add_y))

    def set_camera_pos(self, pos):
        self.__camera_pos = (int(pos[0]), int(pos[1]))

    def set_pitch_dims(self, dims):
        self.__pitch_dims = (int(dims[0]), int(dims[1]), int(dims[2]), int(dims[3]))

    def set_rotate(self, rotate, add_r):
        self.__rotate = float(rotate) + add_r

    def set_camera_height(self, height):
        self.__camera_height = float(height)

    def set_robot_height(self, height):
        self.__robot_height = float(height)

    def set_cropAroundBy(self, crop_around_by):
        self.__crop_around_by = int(crop_around_by)



    # Increment parameters
    def increment_crop_x(self):
        position = self.get_crop_position()
        self.set_crop_position(position, 1, 0)

    def increment_crop_y(self):
        position = self.get_crop_position()
        self.set_crop_position(position, 0, 1)

    def increment_crop_h(self):
        size = self.get_crop_size()
        self.set_crop_size(size, 1, 0)

    def increment_crop_w(self):
        size = self.get_crop_size()
        self.set_crop_size(size, 0, 1)

    def increment_rotate(self, step):
        rotate = self.get_rotate()
        self.set_rotate_(rotate, 0.3)




    # Decrement parameters
    def decrement_crop_x(self):
        position = self.get_crop_position()
        self.set_crop_position(position, -1, 0)

    def decrement_crop_y(self):
        position = self.get_crop_position()
        self.set_crop_position(position, 0, -1)

    def decrement_crop_h(self):
        size = self.get_crop_size()
        self.set_crop_size(size, -1, 0)

    def decrement_crop_w(self):
        size = self.get_crop_size()
        self.set_crop_size(size, 0, -1)

    def decrement_rotate_r(self, step):
        rotate = self.get_rotate()
        self.set_rotate_(rotate, -0.3)



    # Print parameters
    def printCropping(self):
        print "Current cropping parameters: "
        print "[x,y] = ", self.get_crop_position()
        print "[h'w] = ", self.get_crop_size()
        print "[ r ] = ", self.get_rotate()



    def printColour(self):
        print "Current colour parameters are: "
        print "[h,s,v] =   ", self.get_colour()
        print "Tolerance = ", self.get_tolerance()
        print "Noise = ", self.get_noise()



    # Other functions
    def set_parameters_to_full_image(self):
        self.set_crop_x(0)
        self.set_crop_y(0)
        self.set_crop_w(640)
        self.set_crop_h(480)
        self.set_rotate_r(0)



