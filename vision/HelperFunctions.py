import operator
from math import *
from SimpleCV import *



# Calculate the distance between two points
def euclideanDistance(point_1, point_2):
    
    # Float
    return sqrt(sum(map (lambda (x, y): (x-y)*(x-y), \
        zip(point_1, point_2))))



# Return True if the two points are close enough
def closenessCheck(point_1, point_2, tolerance):
    
    # Boolean
    return euclideanDistance(point_1,point_2) <= tolerance



# Return True if the value of the pixel is close enough to the given
def pixelValueCheck(img, point, given_value, tolerance):
    toleranceUpValues = map(lambda x: x+tolerance, given_value)
    toleranceDownValues = map(lambda x: x-tolerance, given_value)
    pixelValue = img.getPixel(point[0], point[1])
    
    # Boolean
    return pixelValue <= toleranceUpValues and pixelValue >= toleranceDownValues



# Normalize a vector
def makeUnitVec(vector):
    scalar = sqrt(1/(vector[0]**2+vector[1]**2))
    
    # Vector
    return map(lambda x: x*scalar, vector)



# Translate coordinates to account for object/camera height
def translateCoordinates (mx, cx, ratio):
    
    # Float
    return (cx-mx) * ratio + mx



