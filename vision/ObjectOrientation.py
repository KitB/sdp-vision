from SimpleCV import *
from Filtering import *
from HelperFunctions import *



# Return the angle between two points
def get_obj_orientation(point_from, point_to):
    
    # If the conditions are satisfied
    if point_from and point_to:
        
        # Get the angle between the two points and the x-axis
        vector = map(operator.sub, point_from, point_to)
        
        # Float (radians)
        return atan2(vector[1], vector[0])
    return None



