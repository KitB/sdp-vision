import cv
import operator
from SimpleCV import *
from Filtering import *
from numpy import average
from HelperFunctions import *



# Split a background image into segments. Average the value of each segment.
# Store each average, together with the boundaries defining the segment.
def set_segment_average(img, obj):
    
    # Information for spliting the image
    obj.erase_values()
    columns = obj.get_columns()
    rows = obj.get_rows()
    (width, height) = img.size()
    column_distance = width/columns
    row_distance = height/rows
    
    # Spliting the image and calculating the average value
    for column in columns:
        for row in rows:
            top_left_x = (column-1)*column_distance
            top_left_y = (row-1)*row_distance
            bot_right_x = column*column_distance
            bot_right_y = row*row_distance
            
            croped_img = img.crop(top_left_x, top_left_y,
                                  bot_right_x, bot_right_y, centered=False)
                                   
            (v,s,h) = img.splitChannels()
            average_value = average(v.getNumpy())
            
            name = "colrow" + str(column) + str(row)
            
            obj.write_value(name, top_left_x, top_left_y, bot_right_x, bot_right_y, average_value)



# Binarize the image, showing desired object
# obj <- instance of Recognizable Object
def get_obj_image(img, obj):
    
    (val, sat, hue) = obj.get_colour()
    (tol_val, tol_sat, tol_hue) = obj.get_tolerance()
    noise  = obj.get_noise()
    
    minHue = (hue - tol_hue)
    maxHue = (hue + tol_hue)
    minSat = sat - tol_sat
    maxSat = sat + tol_sat
    minVal = val - tol_val
    maxVal = val + tol_val
    (v,s,h) = img.splitChannels()
    
    # Image
    return (h.binarize(maxHue) & v.binarize(maxVal) & s.binarize(maxSat) ) & (h.binarize(minHue) |  v.binarize(minVal) | s.binarize(minSat)).invert().erode(noise).dilate(noise)



# Return the blob of the desired object 
def get_obj_blob_coords(obj_img, obj, robot_centroid, robot_binary_image, isCircle):
    
    # Make robot blob bigger and circle blob smoother
    if robot_binary_image and isCircle:
        obj_img = obj_img.dilate(1).erode(1)
        robot_binary_image = robot_binary_image.dilate(3)
    
    # Find all blobs. [Smaller -> Bigger]
    blobs = obj_img.findBlobs()
    
    if blobs:
        
        # Reverse order if looking for a robot. [Bigger -> Smaller]
        if not isCircle:
            blobs.reverse()
        
        for blob in blobs:
            
            # Case 1: Circles
            if robot_centroid and robot_binary_image and isCircle:
                circle_centroid = satisfiesCircleProps(blob, robot_centroid, obj)
                if circle_centroid and satisfiesOrientationProps(circle_centroid, robot_centroid, robot_binary_image, obj):
                    
                    # Point
                    return circle_centroid
            
            # Case 2: Robot/Ball
            elif not isCircle:
                
                # Point
                return satisfiesAreaProps(blob,obj)

    return None



